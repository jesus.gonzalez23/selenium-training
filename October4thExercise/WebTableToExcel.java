import com.excel.lib.util.Xls_Reader;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class WebTableToExcel {

    @Test
    public void importToExcel(){
        System.setProperty("webdriver.chrome.driver", "/home/jesus/Downloads/chromedriver_linux64/chromedriver");
        WebDriver chromeDriver = new ChromeDriver();
        By table = By.id("countires");

        chromeDriver.get("https://cosmocode.io/automation-practice-webtable/");

        String xPath1 = "/html/body/div[2]/div/div/main/article/div/div/table/tbody/tr[";
        String xPath2 = "]/td[";
        String fullXPath = "";

        Xls_Reader excelReader = new Xls_Reader("/home/jesus/Documents/credentials.xlsx");

        excelReader.addSheet("Countries");

        List <WebElement> rows = chromeDriver.findElements(table);
        int totalRows = rows.size();

        for (int j=2; j<=5; j++) {
            for (int i = 2; i <= totalRows; i++) {
                fullXPath = xPath1 + i + xPath2 + j + "]";
                //System.out.print(chromeDriver.findElement(By.xpath(fullXPath)).getText() + " ");
                if (j==2)
                    excelReader.setCellData("Countries","Country",i,chromeDriver.findElement(By.xpath(fullXPath)).getText());
                else if (j==3)
                    excelReader.setCellData("Countries","Capital(s)",i,chromeDriver.findElement(By.xpath(fullXPath)).getText());
                else if (j==4)
                    excelReader.setCellData("Countries","Currency",i,chromeDriver.findElement(By.xpath(fullXPath)).getText());
                else  if (j==5)
                    excelReader.setCellData("Countries","Primary Language(s)",i,chromeDriver.findElement(By.xpath(fullXPath)).getText());

            }
        }

    }
}
