import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class ScreenShot {
    @Test
    public void takeScreenShot(){
        System.setProperty("webdriver.chrome.driver", "/home/jesus/Downloads/chromedriver_linux64/chromedriver");
        WebDriver chromeDriver = new ChromeDriver();
        chromeDriver.get("https://cosmocode.io/automation-practice-webtable/");
        chromeDriver.manage().window().fullscreen();
        //File screenshotFile = null;
        //screenshotFile = ((TakesScreenshot) chromeDriver).getScreenshotAs(OutputType.FILE); ------> A screenshot for a visible frame
        Screenshot fullPageScreenShot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(chromeDriver);
        try {
            //FileUtils.copyFile(screenshotFile, new File("/home/jesus/Pictures/screenShot.png"));
            ImageIO.write(fullPageScreenShot.getImage(),"PNG",new File("/home/jesus/Pictures/fullPageScreenshot.png"));
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        chromeDriver.close();
    }
}
