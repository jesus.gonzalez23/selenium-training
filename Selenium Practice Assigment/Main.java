import com.shoppractice.TestCases.*;

public class Main {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "/home/jesus/Downloads/chromedriver_linux64/chromedriver");
        System.setProperty("webdriver.gecko.driver","/home/jesus/Downloads/geckodriver-v0.30.0-linux64/geckodriver");

        TCLogin1 testCaseLogin1 = new TCLogin1();
        TCLogin2 testCaseLogin2 = new TCLogin2();
        TCFilterItem1 testCaseFilterItem1 = new TCFilterItem1();
        TCFilterItem2 testCaseFilterItem2 = new TCFilterItem2();
        TCPayment1 testCaseDoPayment1 = new TCPayment1();
        TCPayment2 testCaseDoPayment2 = new TCPayment2();

        //------------------------ Login Test Cases --------------------------
        testCaseLogin1.testLoginChrome(); // TC-Login_01 Test on Google Chrome Browser
        testCaseLogin1.testLoginFireFox(); // TC-Login_01 Test on Firefox Browser

        testCaseLogin2.testLoginChrome(); // TC-Login_02 Test on Google Chrome Browser
        testCaseLogin2.testLoginFireFox(); // TC-Login_02 Test on Firefox Chrome Browser

        //--------------------- Filter Item Test Cases ------------------------
        testCaseFilterItem1.testFilterItemChrome(); // TC-FilterItem_01 Test on Google Chrome Browser
        testCaseFilterItem1.testFilterItemFireFox(); // TC-FilterItem_01 Test on Firefox Browser

        testCaseFilterItem2.testFilterItemChrome(); // TC-FilterItem_01 Test on Google Chrome Browser
        testCaseFilterItem2.testFilterItemFireFox(); // TC-FilterItem_01 Test on Firefox Browser

        //---------------------- Do Payment Test Cases ------------------------
        testCaseDoPayment1.testDoPaymentChrome();  // TC-DoPayment_01 Test on Google Chrome Browser
        testCaseDoPayment1.testDoPaymentFireFox(); // TC-DoPayment_01 Test on Firefox Browser

        testCaseDoPayment2.testDoPaymentChrome(); // TC-DoPayment_01 Test on Google Chrome Browser
        testCaseDoPayment2.testDoPaymentFireFox(); // TC-DoPayment_01 Test on Firefox Browser
    }
}
