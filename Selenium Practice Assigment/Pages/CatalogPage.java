package com.shoppractice.Pages;

import org.openqa.selenium.WebDriver;

public class CatalogPage {
    WebDriver driver;

    public CatalogPage(WebDriver driver) {
        this.driver = driver;
    }

    public void loadPage(){
        driver.get("http://automationpractice.com/index.php?id_category=3&controller=category");
    }
}
