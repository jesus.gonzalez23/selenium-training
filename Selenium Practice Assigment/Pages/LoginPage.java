package com.shoppractice.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class LoginPage {
    WebDriver webDriver;

    By username = By.id("email");
    By password = By.id("passwd");
    By signInButton = By.id("SubmitLogin");

    public LoginPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void loginAction(String email, String passwd) {
        webDriver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
        webDriver.findElement(username).sendKeys(email);
        webDriver.findElement(password).sendKeys(passwd);
        webDriver.findElement(signInButton).click();
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.quit();
    }
}
