package com.shoppractice.Pages;

import com.excel.lib.util.Xls_Reader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class LoginPage {
    WebDriver webDriver;

    By username = By.id("email");
    By password = By.id("passwd");
    By signInButton = By.id("SubmitLogin");

    public LoginPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void loginAction() {
        Xls_Reader excelReader = new Xls_Reader("/home/jesus/Documents/credentials.xlsx");
        String sheetName = "login";
        String credentials;

        webDriver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
        credentials = excelReader.getCellData(sheetName,"username",1);
        webDriver.findElement(username).sendKeys(credentials);
        credentials = excelReader.getCellData(sheetName,"password",1);
        webDriver.findElement(password).sendKeys(credentials);
        webDriver.findElement(signInButton).click();
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //webDriver.quit();
    }
}
