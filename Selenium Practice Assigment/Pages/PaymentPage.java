package com.shoppractice.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaymentPage {
    WebDriver webDriver;

    public PaymentPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void loadPage() {
        webDriver.get("http://automationpractice.com/index.php?controller=order&multi-shipping=");
    }
}
