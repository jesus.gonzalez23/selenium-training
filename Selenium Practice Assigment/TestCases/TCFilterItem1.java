package com.shoppractice.TestCases;

import com.shoppractice.Pages.CatalogPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class TCFilterItem1 {
    By topsCheckBox = By.id("layered_category_4");
    By smallSizeCheckBox = By.name("layered_id_attribute_group_1");
    By blueColorCheckBox = By.id("layered_id_attribute_group_14");

    public void testFilterItemChrome(){
        WebDriver chromeDriver = new ChromeDriver();
        CatalogPage catalog = new CatalogPage(chromeDriver);

        catalog.loadPage();
        chromeDriver.findElement(topsCheckBox).click();
        chromeDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        chromeDriver.findElement(smallSizeCheckBox).click();
        chromeDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        chromeDriver.findElement(blueColorCheckBox).click();
        chromeDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        chromeDriver.quit();
    }
    public void testFilterItemFireFox(){
        WebDriver fireFoxDriver = new FirefoxDriver();
        CatalogPage catalog = new CatalogPage(fireFoxDriver);

        catalog.loadPage();
        fireFoxDriver.findElement(topsCheckBox).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        fireFoxDriver.findElement(smallSizeCheckBox).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        fireFoxDriver.findElement(blueColorCheckBox).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        fireFoxDriver.quit();

    }
}
