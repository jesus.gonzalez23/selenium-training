package com.shoppractice.TestCases;

import com.shoppractice.Pages.CatalogPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class TCFilterItem2 {
    By dressesCheckBox = By.id("layered_category_4");
    By mediumSizeCheckBox = By.name("layered_id_attribute_group_1");
    By blackColorCheckBox = By.id("layered_id_attribute_group_14");
    By cottonCompCheckBox = By.xpath("//*[@id=\"layered_id_feature_5\"]");

    public void testFilterItemChrome(){
        WebDriver chromeDriver = new ChromeDriver();
        CatalogPage catalog = new CatalogPage(chromeDriver);

        catalog.loadPage();
        chromeDriver.findElement(dressesCheckBox).click();
        chromeDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        chromeDriver.findElement(mediumSizeCheckBox).click();
        chromeDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        chromeDriver.findElement(blackColorCheckBox).click();
        chromeDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        chromeDriver.findElement(cottonCompCheckBox).click();
        chromeDriver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        chromeDriver.quit();
    }
    public void testFilterItemFireFox(){
        WebDriver fireFoxDriver = new FirefoxDriver();
        CatalogPage catalog = new CatalogPage(fireFoxDriver);

        catalog.loadPage();
        fireFoxDriver.findElement(dressesCheckBox).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        fireFoxDriver.findElement(mediumSizeCheckBox).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        fireFoxDriver.findElement(blackColorCheckBox).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        fireFoxDriver.findElement(cottonCompCheckBox).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        //fireFoxDriver.quit();
    }
}
