package com.shoppractice.TestCases;

import com.shoppractice.Pages.LoginPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TCLogin1 {

     public void testLoginChrome(){
        WebDriver chromeDriver = new ChromeDriver();
        LoginPage login = new LoginPage(chromeDriver);

        login.loginAction("test-user@mail.com","test-password1");
    }

    public void testLoginFireFox(){
        WebDriver fireFoxDriver = new FirefoxDriver();
        LoginPage login = new LoginPage(fireFoxDriver);

        login.loginAction("test-user@mail.com","test-password1");
    }
}
