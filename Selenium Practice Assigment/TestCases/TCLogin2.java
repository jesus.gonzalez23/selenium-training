package com.shoppractice.TestCases;

import com.shoppractice.Pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TCLogin2 {
    public void testLoginChrome(){
        WebDriver chromeDriver = new ChromeDriver();
        LoginPage login = new LoginPage(chromeDriver);

        login.loginAction("no-registered-email@mail.com","no-registered-password");
    }

    public void testLoginFireFox() {
        WebDriver fireFoxDriver = new FirefoxDriver();
        LoginPage login = new LoginPage(fireFoxDriver);

        login.loginAction("no-registered-email@mail.com", "no-registered-password");
    }
}
