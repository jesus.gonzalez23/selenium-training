package com.shoppractice.TestCases;

import com.shoppractice.Pages.PaymentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class TCPayment2 {
    By payByCheckButton = By.className("cheque");
    By confirmOrderButton = By.className("button btn btn-default button-medium");

    public void testDoPaymentChrome() {
        WebDriver chromeDriver = new ChromeDriver();
        PaymentPage payment = new PaymentPage(chromeDriver);
        WebDriverWait wait = new WebDriverWait(chromeDriver,30);

        payment.loadPage();
        chromeDriver.findElement(payByCheckButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(confirmOrderButton));
        chromeDriver.findElement(confirmOrderButton).click();
        chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        chromeDriver.quit();
    }
    public void testDoPaymentFireFox() {
        WebDriver fireFoxDriver = new FirefoxDriver();
        PaymentPage payment = new PaymentPage(fireFoxDriver);
        WebDriverWait wait = new WebDriverWait(fireFoxDriver,30);

        payment.loadPage();
        fireFoxDriver.findElement(payByCheckButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(confirmOrderButton));
        fireFoxDriver.findElement(confirmOrderButton).click();
        fireFoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        fireFoxDriver.quit();
    }
}
