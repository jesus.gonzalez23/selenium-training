import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

public class KeyboardAndMouseInput {
    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "/home/jesus/Downloads/chromedriver_linux64/chromedriver");

        WebDriver driver = new ChromeDriver();

        driver.get("https://www.google.com/");

        WebElement name = driver.findElement(By.name("q"));
        WebElement search = driver.findElement(By.name("btnK"));
        name.click();
        name.sendKeys("tcs");
        name.submit();
        driver.quit();
    }


}
