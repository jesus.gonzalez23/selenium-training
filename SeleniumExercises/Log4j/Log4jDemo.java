import utils.Log;

public class Log4jDemo {

    public static void main(String[] args){
        Log logger = new Log();

        System.out.println("Welcome, this is a logging example...\n");
        logger.info("This is an info message...");
        logger.error("This is an error message...");
        logger.warn("This is a warning message...");
        logger.fatal("This is a fatal message...");

        System.out.println("\nCompleted!");
    }
}
