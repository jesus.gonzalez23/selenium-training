import java.util.Scanner;

public class BreakInteger {
    public static void main (String[] args) {
        int number;
        String numberStr;
        Scanner input = new Scanner(System.in);

        System.out.println("*** This program trims an integer number and prints out its digits in sequence ***");
        System.out.print("\nEnter a number -> ");
        number = input.nextInt();
        numberStr = Integer.toString(number);

        for (int i=0; i<numberStr.length(); i++) {
            System.out.print(numberStr.charAt(i)+" ");
        }
    }
}
