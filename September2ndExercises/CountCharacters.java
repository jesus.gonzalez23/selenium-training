import java.util.Scanner;

public class CountCharacters {
    public static void main (String[] args) {
        String phrase;
        Scanner input = new Scanner(System.in);

        System.out.println("*** This program counts the amount of characters from a string ***");
        System.out.print("\nEnter a phrase -> ");
        phrase = input.nextLine();
        System.out.println("Your string ''" + phrase + "'' has " + phrase.length() + " characters.");
    }
}
