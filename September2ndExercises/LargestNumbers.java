import java.util.Arrays;
import java.util.Scanner;

public class LargestNumbers {
    public static void main(String[] args) {
        int[] numbers = {23,31,10,95,9,55,100,1};
        Arrays.sort(numbers);
        System.out.println("The largest elements of the given array are:");
        for (int i=1; i<=3; i++)
            System.out.print(numbers[numbers.length-i] + " ");
    }
}
