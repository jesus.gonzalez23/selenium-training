public class ExceptionHandling {
    public static void main(String[] args) {

        /*There are 2 types of errors in Java, compiling exceptions and execution exceptions. When an execution
        exception occurs, the program will crash and stop. To avoid for a program being interrupted the "try ... catch"
        sentence allows handle execution errors, in this way the error can be notified to the user but the program
        will remain running. */

        int[] numbers = {10,20,30,40,50};

        for (int i=0; i<6; i++) {
            try {
                System.out.println(numbers[i]+" ");
            }
            catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("\niterator is out of bounds from the array index!!!");
                System.out.println("Array index: "+(numbers.length-1)+ "  iterator: "+i);
                System.out.println("numbers["+i+"] does not exit.");
            }
        }
        System.out.println("\nExecution exception handled, program will finish in 3...2...1...");
    }
}
