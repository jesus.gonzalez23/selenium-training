public class Main {
    public static void main(String[] args) {

        //Polymorphism consists when a super class instance can take the behavior of its inherited classes
        Device desktop1 = new DesktopPC();
        Device mobilePhone1 = new MobilePhone();
        Device gamingConsole1 = new GamingConsole();
        Device smartTV1 = new SmartTV();

        desktop1.turnOn();
        mobilePhone1.turnOn();
        gamingConsole1.turnOn();
        smartTV1.turnOn();

        /*In fact, when applying polymorphism, there is inheritance as well, however inheritance allow us reuse
        code and attributes from the super class. Keep in mind that instances must be from the inherited classes
        since super class attribute and methods can be reached from the subclass.
         */
        DesktopPC desktop2 = new DesktopPC();
        MobilePhone mobilePhone2 = new MobilePhone();
        GamingConsole gamingConsole2 = new GamingConsole();
        SmartTV smartTV2 = new SmartTV();

        desktop2.storage = "1 TB";
        mobilePhone2.storage = "128 GB";
        gamingConsole2.storage = "1 TB";
        smartTV2.storage = "16 GB";
    }
}
